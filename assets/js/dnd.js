const potato = document.getElementsByClassName('potatoes')[0]
const baskets = document.getElementsByClassName('basket')

// Passa por cada container e adiciona um eventListener para um elemento sendo
// arrastado por cima, para dentro e sendo solto em cima do container.
for (const basket of baskets) {
    basket.addEventListener("dragover", dragover)
    basket.addEventListener("dragenter", dragenter)
    basket.addEventListener("drop", drop)
}

function dragover(e) {
    e.preventDefault()
}
function dragenter(e) {
    e.preventDefault()
}
function drop() {
    this.append(potato)
}

/*
https://medium.com/quick-code/simple-javascript-drag-drop-d044d8c5bed5
Obrigada David Brennan por me mostrar q era 87x mais simples do que eu pensei
*/