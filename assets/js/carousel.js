$(document).ready(function() {
    $('.owl-carousel').owlCarousel({
        items: 3,
        margin: 15,
        autoplay: true,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
    });
})